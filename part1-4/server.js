'use strict';
const express = require('express');
const moment = require('moment');
const util = require('util');
const fs = require('fs');
const responseTime = require('response-time');

const logFile = fs.createWriteStream('errors.log', {flags: 'a'});
const logStdout = process.stdout;

console.log = (d) => {
  logFile.write(moment().format() + ' | ' + util.format(d) + '\n');
  logStdout.write(util.format(d) + '\n');
};
console.error = console.log;

const app = express();

// add a X-Response-Time header to responses
app.use(responseTime());

let users = [
    {
        _id: 453534534534545475,
        firstname: 'Robin',
        lastname: 'Zachmann',
        email: 'robin.zachmann@gmail.com',
    },
    {
        _id: 5677887909,
        firstname: 'Florian',
        lastname: 'Schaller',
        email: 'flo.schaller@gmail.com',
    },
    {
        _id: 5679879098090,
        firstname: 'John',
        lastname: 'Doe',
        email: 'john.doe@gmail.com',
    },
];

const requestHeader = (req, res, next) => {
    console.log('path: ' + req.path);
    console.log('timestamp: ' + moment());
    console.log('client ip: ' + req.ip);
    console.log('user-agent: ' + req.get('User-Agent'));
    next();
};

app.use((err, req, res, next) => {
    res.status(404);
    console.error(err);
    res.render('error', {error: err});
});

app.route('/users/:userId')
    .get(requestHeader, (req, res, next) => {
        const userId = req.params.userId;
        const user = users.filter((user) => user._id == userId)[0];
        if(user === undefined) {
            return next(`User '${userId}' does not exist!`);
        }
        res.json(user);
    })
    .put((req, res, next) => {
        const userId = req.params.userId;
        const user = users.filter((user) => user._id == userId)[0];
        if(user === undefined) {
            return next(`User '${userId}' does not exist!`);
        }
        users = users.filter((user) => user._id !== userId);
        user.firstname = 'John';
        user.lastname = 'Doe';
        users.push(user);
    })
    .delete((req, res) => {
        const userId = req.params.userId;
        users = users.filter((user) => user._id !== userId);
    });

app.post('/users', (req, res) => {
    const newUser = {
        _id: 45667678,
        firstname: 'New',
        lastname: 'User',
        email: 'new.user@gmail.com',
    };
    users.push(newUser);
});

console.log('Server listening on port 3000');
app.listen(3000);
