var express = require('express');
var router = express.Router();

const shoppingItems = [
{
    '_id': '58dd240bb74336091068d20e',
    'name': 'Ketchup',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:28:11.838+0000',
    'group': '58dd2b7fb74336091068d219',
    'members': [
        '58dc06ea33017e2d0849ff62',
        '58dd200c8526922788e9c1bd',
    ]
},
{
    '_id': '58dd2440b74336091068d20f',
    'name': 'Mayo',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:29:04.015+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
        '58dd200c8526922788e9c1bd',
    ]
},
{
    '_id': '58dd2450b74336091068d210',
    'name': 'Hot Dog sausages',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:29:20.834+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
        '58dd200c8526922788e9c1bd',
    ]
},
{
    '_id': '58dd245db74336091068d211',
    'name': 'Chicken',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:29:33.823+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
        '58dd200c8526922788e9c1bd',
    ]
},
{
    '_id': '58dd2469b74336091068d212',
    'name': 'Beers',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:29:45.830+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
        '58dd200c8526922788e9c1bd',
    ]
},
{
    '_id': '58dd2474b74336091068d213',
    'name': 'Tooth paste',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:29:56.504+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
    ]
},
{
    '_id': '58dd2481b74336091068d214',
    'name': 'Shampoo',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:30:09.050+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
    ]
},
{
    '_id': '58dd2488b74336091068d215',
    'name': 'Coke bottle (1,5l)',
    'creator': '58dc06ea33017e2d0849ff62',
    'added': '2017-03-30T15:30:16.773+0000',
    'group': '58dd21e18526922788e9c1bf',
    'members': [
        '58dc06ea33017e2d0849ff62',
    ]
}];

/* GET shopping items list */
router.get('/', (req, res) => {
  res.json(shoppingItems);
});

/* GET shopping item by ID */
router.get('/:id', (req, res, next) => {
    const shoppingItemId = req.params.id;
    const item = shoppingItems.filter((item) => item._id == shoppingItemId)[0];
    if(item === undefined) {
        return next(`Shopping item #'${shoppingItemId}' does not exist!`);
    }
    res.json(item);
});

module.exports = router;
