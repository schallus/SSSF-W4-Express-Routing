var express = require('express');
var router = express.Router();

const users = [{
    '_id': '58dc06ea33017e2d0849ff62',
    'firstname': 'Florian',
    'lastname': 'Schaller',
    'nickname': 'schallus',
    'email': 'florian.schaller@metropolia.fi',
    'passwordHash': '$5$MnfsQ4iN$ZMTppKN16y/tIsUYs/obHlhdP.Os80yXhTurpBMUbA5',
    'image': 'http://placekitten.com/200/300',
},
{
    '_id': '58dd200c8526922788e9c1bd',
    'firstname': 'Robin',
    'lastname': 'Zachmann',
    'nickname': 'athenos',
    'email': 'robin.zachmann@metropolia.fi',
    'passwordHash': '$5$MnfsQ4iN$ZMTppKN16y/tIsUYs/obHlhdP.Os80yXhTurpBMUbA5',
    'image': 'http://placekitten.com/200/300',
},
{
    '_id': '58dd203b8526922788e9c1be',
    'firstname': 'Clément',
    'lastname': 'Schaller',
    'nickname': 'moussepousse',
    'email': 'schallus.clement@outlook.com',
    'passwordHash': '$5$MnfsQ4iN$ZMTppKN16y/tIsUYs/obHlhdP.Os80yXhTurpBMUbA5',
    'image': 'http://placekitten.com/200/300',
}];

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json(users);
});

router.get('/:userId', (req, res, next) => {
  const userId = req.params.userId;
  const user = users.filter((user) => user._id == userId)[0];
    if(user === undefined) {
        return next(`User #'${userId}' does not exist!`);
    }
    res.json(user);
});

module.exports = router;
