# Express Routing - Week 4 assignment

## Assignment 1: Rest revisited
[Link to the code](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/master/part1-4/server.js)

## Assignment 2: Middleware
I used the same project to test those middleware.
[Link to the code](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/master/part1-4/server.js)

## Assignment 3: Error handling
I used the same project to test those middleware.
[Link to the code](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/master/part1-4/server.js)

## Assignment 4: Middleware libraries
For this part, I decided to use [response-time](https://www.npmjs.com/package/response-time) which allow you to add a X-Response-Time header to responses. This would be useful in our project to optimise our request and make them as quick as possible.
I used the same project to test this middleware.
[Link to the code](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/master/part1-4/server.js)

## Assignment 5: Routes in separate modules

Implement at least two different route-modules
* [users](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/3ec1d3c56df2bd832bbf089a6fa9f3704cb03af7/part5/routes/users.js)
* [shoppingItems](https://gitlab.com/schallus/SSSF-W4-Express-Routing/blob/3ec1d3c56df2bd832bbf089a6fa9f3704cb03af7/part5/routes/shopping.js)